#!/bin/bash

echo -n "Earthdata username: "
read EARTHDATA_USER

echo -n "Earthdata password: "
read -s EARTHDATA_PASS

wget --http-user="$EARTHDATA_USER" --http-password="$EARTHDATA_PASS" \
     --load-cookies ~/.urs_cookies --save-cookies ~/.urs_cookies --keep-session-cookies --no-check-certificate \
     --auth-no-challenge -r --reject "index.html*" -np --no-host-directories -e robots=off \
     "https://n5eil01u.ecs.nsidc.org/SMAP/SPL4SMGP.003/2015.03.31/"
wget --http-user="$EARTHDATA_USER" --http-password="$EARTHDATA_PASS" \
     --load-cookies ~/.urs_cookies --save-cookies ~/.urs_cookies --keep-session-cookies --no-check-certificate \
     --auth-no-challenge -r --reject "index.html*" -np --no-host-directories -e robots=off \
     "https://n5eil01u.ecs.nsidc.org/SMAP/SPL4SMGP.003/2015.04.01/"
