#!/bin/bash

INOTIFY_LOG="./inotify.log"
touch $INOTIFY_LOG
INOTIFY_LOG=$(readlink -f $INOTIFY_LOG)

echo "Writing inotify events to $INOTIFY_LOG. To watch: tail -f $INOTIFY_LOG"
inotifywait -rm --timefmt '%F %T' --format '%T %w %e %f' ./besd-tmp > $INOTIFY_LOG 2>&1 &
