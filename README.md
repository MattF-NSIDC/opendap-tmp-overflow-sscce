AppEEARS OPeNDAP
================

This is the development README. For runtime instructions see
[the runtime README](opendap/src/master/source/README.md).

Overview
--------

This project provisions an OPeNDAP stack and provides tools for reproducing an
issue: https://github.com/OPENDAP/bes/issues/39

Usage
-----

This system is comprised of 2 docker containers: 'olfs', 'besd'.
The container status can be verified by running `docker ps -a`. It is also
possible to manually restart the containers with:

```
cd /vagrant/docker
docker-compose stop
docker-compose up -d
```

The opendap service is accessible at `http://<host>/opendap`.

Reproducing the issue
---------------------

All steps below assume you start from the root of this repo.

1) Get some data:

    cd docker/data
    ./get_data.sh

2) Optionally start inotifywait and watch its output:

    cd docker
    ./inotify.sh
    tail -f inotify.log

3) Run success case test:

    cd test
    ./test_cleanup_success.sh

4) You should see no new tempfiles in docker/besd-tmp

5) Run failure case test:

    cd test
    ./test_cleanup_failure.sh

6) You should see a 1.7GB tempfile in docker/besd-tmp
