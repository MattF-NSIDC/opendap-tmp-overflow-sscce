test_url="http://127.0.0.1/opendap/aggregates/h00v00.ncml.nc?Geophysical_Data_baseflow_flux[0:1:0][0:1:7][0:1:1623][0:1:3855],Geophysical_Data_heat_flux_ground[0:1:0][0:1:7][0:1:1623][0:1:3855],Geophysical_Data_heat_flux_latent[0:1:0][0:1:7][0:1:1623][0:1:3855],Geophysical_Data_heat_flux_sensible[0:1:0][0:1:7][0:1:1623][0:1:3855],Geophysical_Data_height_lowatmmodlay[0:1:0][0:1:7][0:1:1623][0:1:3855],Geophysical_Data_land_evapotranspiration_flux[0:1:0][0:1:7][0:1:1623][0:1:3855],Geophysical_Data_land_fraction_saturated[0:1:0][0:1:7][0:1:1623][0:1:3855],Geophysical_Data_land_fraction_snow_covered[0:1:0][0:1:7][0:1:1623][0:1:3855],Geophysical_Data_land_fraction_unsaturated[0:1:0][0:1:7][0:1:1623][0:1:3855]"
curl --globoff "${test_url}" > /dev/null 2>&1 &
PID=$!
echo "Requesting ridiculously large amount of data (PID $PID)..."
echo "Killing request in 50 seconds..."

sleep 50
kill $PID
echo "Request killed. Check docker/besd-tmp for pollution."
